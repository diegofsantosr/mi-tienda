<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="icon" href="img/mitienda.png"/>
        <title>Inicio de Sesi�n</title>
    </head>
    <body>
        <header class="header">
            <img class="header__img" src="img/fototienda.jpg" alt="logo">            
        </header>        
        <section class="login">
            <section class="login__container">
                <h2>Inicia Sesi�n</h2>
                <form class="login__container--form" >
                    <input class="input" type="text" placeholder="Usuario">
                    <input class="input" type="password" placeholder="Contrase�a">
                    <button class="button">Iniciar Sesi�n</button>                    
                    <spam style="color:red"><%=request.getAttribute("errMessaje")== null? "":request.getAttribute("errMessaje")%></spam>

                </form>
            </section>
        </section>
        <footer class="footer">
            <a href="/">Terminos de uso</a>
            <a href="/">Declaracion de privacidad</a>
            <a href="/">Centro de ayuda</a>
        </footer>
    </body>
</html>
