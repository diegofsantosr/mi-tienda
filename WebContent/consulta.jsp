<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Consulta</title>
</head>
<body>
<% if(session.getAttribute("username")==null)
	response.sendRedirect(request.getContextPath() + "/login.jsp");
	
%>
    <form action="listarProductosNombre" method="post">
        <label>Nombre</label><input type="text" name="nombre" /><br /> <br />
        <input type="submit" value="Buscar" />
    </form>
    <br>
    <hr>
    <br>

   <table class="table table-dark">
       <thead>
        <tr scope="row">
            <th scope="col">Codigo</th>
            <th scope="col">Nombre</th>
            <th scope="col">Descripcion</th>
            <th scope="col">Fecha Vencimiento</th>
            <th scope="col">Precio Compra</th>
            <th scope="col">Precio Venta</th>
            <th scope="col">Cantidad</th>
        </tr>
       </thead>
       <tbody>
			<c:forEach var="producto" items="${listaProductos}">
				<tr>
					<td><c:out value="${producto.codigo}"/></td>
					<td><c:out value="${producto.nombre}"/></td>
					<td><c:out value="${producto.descripcion}"/></td>
					<td><c:out value="${producto.fecha_vencimiento}"/></td>
					<td><c:out value="${producto.precio_compra}"/></td>
					<td><c:out value="${producto.precio_venta}"/></td>
                                        <td><c:out value="${producto.cantidad}"/></td>
					<td><a href="deleteProducto?id=<c:out value="${producto.nombre}"/>" value="eliminar" />Eliminar</a></td>
				</tr>
 			</c:forEach>
       </tbody>    
       
   </table> 

</body>
</html>