package com.mintic.s5.sixersteam.tienda.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mintic.s5.sixersteam.tienda.model.ConnectionManager;
import com.mintic.s5.sixersteam.tienda.model.Usuario;
import java.sql.ResultSet;

public class UsuarioDAO {
	private ConnectionManager con;
	private Connection connection;

	public UsuarioDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) throws SQLException {
		System.out.println(jdbcURL);
		con = new ConnectionManager(jdbcURL, jdbcUsername, jdbcPassword);
	}

	// insertar usuario
	public boolean insertar(Usuario usuario, String contrasena) throws SQLException, ClassNotFoundException  {
		String sql = "INSERT INTO Usuarios ( usuario, contrasena) VALUES (?,?)";
		con.conectar();
		connection = con.getJdbcConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, usuario.getUsuario());
		statement.setString(2, usuario.getContrasena());
		
		statement.executeUpdate();
		System.out.println(statement);

		statement.close();
		con.desconectar();
		return true;
	}
        
        // Validar identidad
	public Usuario validarIdentidad(String usuario, String contrasena) throws SQLException, ClassNotFoundException {

		String sql = "SELECT * FROM usuarios as u WHERE  u.usuario=? and u.contrasena =?";
		con.conectar();
		connection = con.getJdbcConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, usuario);
		statement.setString(2, contrasena);
		System.out.println(statement);
		ResultSet resultado = statement.executeQuery();
		Usuario usuario = new Usuario();
		if (resultado.next()) {
			Usuario.setUsuario(resultado.getString("usuario"));
		}
		resultado.close();
		con.desconectar();
		return usuario;

	}

	    
}
