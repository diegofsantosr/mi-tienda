package com.mintic.s5.sixersteam.tienda.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mintic.s5.sixersteam.tienda.model.ConnectionManager;
import com.mintic.s5.sixersteam.tienda.model.Producto;

public class ProductoDAO {
	private ConnectionManager con;
	private Connection connection;

	public ProductoDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) throws SQLException {
		System.out.println(jdbcURL);
		con = new ConnectionManager(jdbcURL, jdbcUsername, jdbcPassword);
	}

	// insertar producto
	public boolean insertar(Producto producto) throws SQLException, ClassNotFoundException  {
		String sql = "INSERT INTO productos ( codigo, nombre, descripcion, fecha_vencimiento, precio_compra, precio_venta, cantidad) VALUES (?,?,?,?,?,?,?)";
		con.conectar();
		connection = con.getJdbcConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, producto.getCodigo());
		statement.setString(2, producto.getNombre());
		statement.setString(3, producto.getDescripcion());
		statement.setString(4, producto.getFecha_vencimiento());
		statement.setInt(5, producto.getPrecio_compra());
                statement.setInt(6, producto.getPrecio_venta());
		statement.setInt(7, producto.getCantidad());			

		statement.executeUpdate();
		System.out.println(statement);

		statement.close();
		con.desconectar();
		return true;
	}

	// Consultar Producto
	public List<Producto> consultarPorFiltros(String nombre) throws SQLException, ClassNotFoundException {
		List<Producto> consultaResult = new ArrayList<>();

		String sql = "SELECT p.codigo, p.nombre, p.descripcion, p.fecha_vencimiento, p.precio_compra, p.precio_venta, p.cantidad FROM `productos` AS p WHERE p.nombre LIKE ?";
		con.conectar();
		connection = con.getJdbcConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(2, "%" + nombre + "%");
		System.out.println(statement);
		ResultSet resultado = statement.executeQuery();
		while (resultado.next()) {
			Producto producto = new Producto();
			producto.setCodigo(resultado.getString("codigo"));
			producto.setNombre(resultado.getString("nombre"));
			producto.setDescripcion(resultado.getString("descripcion"));
			producto.setFecha_vencimiento(resultado.getString("fecha_vencimiento"));
			producto.setPrecio_compra(resultado.getInt("precio_compra"));
			producto.setPrecio_venta(resultado.getInt("precio_venta"));
			producto.setCantidad(resultado.getInt("cantidad"));
                        
			consultaResult.add(producto);
		}
		resultado.close();
		con.desconectar();
		return consultaResult;

	}
        

	// Eliminar producto
	public void deletePersonByDocument(String nombre) throws SQLException, ClassNotFoundException {

		String sql = "Delete FROM productos as p WHERE p.nombre=?";
		con.conectar();
		connection = con.getJdbcConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, nombre);
		System.out.println(statement);
		statement.executeUpdate();
		con.desconectar();

	}
    
}
