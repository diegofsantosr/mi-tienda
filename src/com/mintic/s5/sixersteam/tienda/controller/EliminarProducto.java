package com.mintic.s5.sixersteam.tienda.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mintic.s5.sixersteam.tienda.dao.ProductoDAO;

@WebServlet("/deleteProducto")
public class EliminarProducto extends HttpServlet {
	
	private static final long serialVersionUID = -4098189182525313098L;


	private ProductoDAO productoDao;


	public EliminarProducto() {
		super();
	}

	@Override
	public void init() {
		System.out.println("Inicio Controller Eliminar");
		
		// Recuperar variables del contexto web.xml
		String jdbcURL = getServletContext().getInitParameter("jdbcURL");
		String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
		String jdbcPassword = getServletContext().getInitParameter("jdbcPassword");

		try {
			// inicializar mi intancia dao. Incluida establecer la conexion
			this.productoDao = new ProductoDAO(jdbcURL, jdbcUsername, jdbcPassword);
		} catch (SQLException e) {
			System.out.println("hubo un problema al conectarme :" + e.getMessage());
		}
		
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			productoDao.deletePersonByDocument(request.getParameter("id"));
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Error");
			System.out.println(e.getMessage());
		}
		request.getRequestDispatcher("consulta.jsp").forward(request, response);
		/*
		 * RequestDispatcher rd = request.getRequestDispatcher("consulta.jsp");
		 * rd.forward(request, response);
		 */
	}


}
