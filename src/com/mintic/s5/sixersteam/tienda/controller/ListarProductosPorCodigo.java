package com.mintic.s5.sixersteam.tienda.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mintic.s5.sixersteam.tienda.dao.ProductoDAO;
import com.mintic.s5.sixersteam.tienda.model.Producto;

/**
 * Servlet implementation class CrudProducto
 */
@WebServlet("/listarProductosDoc")
public class ListarProductosPorCodigo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ProductoDAO productoDao;

	@Override
    public void init() {
    	String jdbcURL = getServletContext().getInitParameter("jdbcURL");
    	String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
		String jdbcPassword = getServletContext().getInitParameter("jdbcPassword");
		
		try {
			productoDao = new ProductoDAO(jdbcURL, jdbcUsername, jdbcPassword);
		} catch (SQLException e) {
			System.out.println("hubo un problema al conectarme :"+ e.getMessage());
		}

    }
	


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Ejecutando Post...");
		List<Producto> productos = null;
		try {
			productos = productoDao.consultarPorFiltros(request.getParameter("doc"));
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Error");
			System.out.println(e.getMessage());
		}
		request.setAttribute("listaProductos", productos);

		getServletContext().getRequestDispatcher("/consulta.jsp").forward(request, response);
	}

}
