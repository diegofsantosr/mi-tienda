package com.mintic.s5.sixersteam.tienda.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mintic.s5.sixersteam.tienda.dao.ProductoDAO;
import com.mintic.s5.sixersteam.tienda.model.Producto;

@WebServlet("/LoginServerlet")
public class LoginServerlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4098189182525313098L;

	/**
	 * 
	 */

	private UsuarioDAO UsuarioDao;

	int cont;


	public LoginServerlet() {
		super();
	}

	@Override
	public void init() {
		System.out.println("Inicio Controller Login");

		// Contador de ingresos fallidos
		cont = 0;

		// Recuperar variables del contexto web.xml
		String jdbcURL = getServletContext().getInitParameter("jdbcURL");
		String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
		String jdbcPassword = getServletContext().getInitParameter("jdbcPassword");

		try {
			// inicializar mi intancia dao. Incluida establecer la conexion
			this.usuarioDao = new UsuarioDAO(jdbcURL, jdbcUsername, jdbcPassword);
		} catch (SQLException e) {
			System.out.println("hubo un problema al conectarme :" + e.getMessage());
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Ejecutando Post...");
		// Control de ingresos Fallidos
		if (cont >= 3) {
			System.out.println("Supero los 3 intentos ");
			response.sendRedirect(request.getContextPath() + "/error.jsp");
		} else {

			String document;
			String password;

			usuario = request.getParameter("usuario");
			password = request.getParameter("password");

			System.out.println(usuario);
			System.out.println(password);

			try {
				Usuario u = usuarioDao.validarIdentidad(usuario, password);

				System.out.println(u.getUsuario());

				if (u.getUsuario() != null) {
					System.out.println("Usuario encontrado");
					
					//Colocar un valor en session 
					request.getSession().setAttribute("username", u.getUsuario() );
					request.getRequestDispatcher("consulta.jsp").forward(request, response);
					
				} else {
					System.out.println("Usuario no encontrado");
					cont++;
					request.setAttribute("errMessaje", "Usuario no encontrado");
					
					request.getRequestDispatcher("login.jsp").forward(request, response);
					//response.sendRedirect(request.getContextPath() + "/login.jsp");
				}

			} catch (SQLException | ClassNotFoundException e) {
				System.out.println("Error");
				System.out.println(e.getMessage());
				
				response.sendRedirect(request.getContextPath() + "/login.jsp");
			}

		}
	}


}
