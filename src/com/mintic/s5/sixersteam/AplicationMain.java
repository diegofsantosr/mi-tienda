package com.mintic.s5.sixersteam;
// Cambios de prueba
import java.sql.SQLException;

import com.mintic.s5.sixersteam.tienda.model.Producto;
import com.mintic.s5.sixersteam.tienda.dao.ProductoDAO;
import com.mintic.s5.sixersteam.tienda.model.Usuario;
import com.mintic.s5.sixersteam.tienda.dao.UsuarioDAO;

public class AplicationMain {
	
	public static void main(String[] args) throws ClassNotFoundException {
		ProductoDAO productoDao;
                UsuarioDAO usuarioDao;
		
    	String jdbcURL = "jdbc:mysql://localhost:3360/mibarrio_db=false";
    	String jdbcUsername = "root";
		String jdbcPassword = "root";
		
		Producto producto = new Producto();
                producto.setCodigo("a2");
		producto.setNombre("Galletas");
		producto.setDescripcion("galletas festival");
                producto.setFecha_vencimiento("311221");
                producto.setPrecio_compra(500);
                producto.setPrecio_venta(1000);
                producto.setCantidad(5);
		
		try {
			productoDao = new ProductoDAO(jdbcURL, jdbcUsername, jdbcPassword);
			if(productoDao.insertar(producto)) {
				System.out.println("Registgro insertado exitosamente");
			}else {
				System.out.println("no se pudo insertar el registro");
			}
		} catch (SQLException e) {
			System.out.println("hubo un problema al conectarme :"+ e.getMessage());
		}
                
                try {
			usuarioDao = new UsuarioDAO(jdbcURL, jdbcUsername, jdbcPassword);
			Usuario usuario = usuarioDao.validarIdentidad("Diego", "df2021");
			if(usuario.getUsuario() != null) {
				System.out.println("Hola "+usuario.getUsuario());
			}
			else {
				System.out.println("La persona no esta resgistrada en el sistema");
			}
		} catch (SQLException e) {
			System.out.println("no se pudo realizar la consulta:"+ e.getMessage());
		}



	}

}
